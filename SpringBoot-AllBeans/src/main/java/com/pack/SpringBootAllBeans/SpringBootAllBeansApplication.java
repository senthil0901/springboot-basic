package com.pack.SpringBootAllBeans;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

//@SpringBootApplication (exclude = SecurityAutoConfiguration.class)
@SpringBootApplication
public class SpringBootAllBeansApplication {

	public static void main(String[] args) {
		 ApplicationContext ctx=SpringApplication.run(SpringBootAllBeansApplication.class, args);
		
	        String[] beanNames = ctx.getBeanDefinitionNames();
	         
	        Arrays.sort(beanNames);
	         
	        for (String beanName : beanNames) 
	        {
	            System.out.println(beanName);
	        }
	}

}
