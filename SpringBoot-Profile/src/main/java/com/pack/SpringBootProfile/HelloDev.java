package com.pack.SpringBootProfile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component //registered as spring bean
@Profile("dev")
public class HelloDev implements HelloInterface {

	public String getGreeting() {
		return "Hello from Dev";
	}

}
