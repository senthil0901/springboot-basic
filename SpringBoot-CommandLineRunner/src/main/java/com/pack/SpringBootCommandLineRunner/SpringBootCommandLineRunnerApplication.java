package com.pack.SpringBootCommandLineRunner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;

@Order(value=1)
@SpringBootApplication
public class SpringBootCommandLineRunnerApplication implements CommandLineRunner {

	protected final Log logger = LogFactory.getLog(getClass());
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootCommandLineRunnerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Application started using commandline runner");
		
	}

}
