
package com.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { TestBeanConfig.class })
public class BasicTest {
	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {

		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void loginTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/login")).andExpect(MockMvcResultMatchers.status().isOk())
				
				.andExpect(MockMvcResultMatchers.view().name("login"));
	}
	@Test
	public void registerTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/register")).andExpect(MockMvcResultMatchers.status().isOk())
				
				.andExpect(MockMvcResultMatchers.view().name("register"));
	}
	@Test
	public void successTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/success")).andExpect(MockMvcResultMatchers.status().isOk())
				
				.andExpect(MockMvcResultMatchers.view().name("success"));
	}
	@Test
	public void welcomeTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/welcome")).andExpect(MockMvcResultMatchers.status().isOk())
				
				.andExpect(MockMvcResultMatchers.view().name("welcome"));
	}

	}