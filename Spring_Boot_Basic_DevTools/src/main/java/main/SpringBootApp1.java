package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class })
public class SpringBootApp1 {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApp1.class, args);
    }
}

/*
 * @Configuration
 * 
 * @EnableAutoConfiguration
 * 
 * @ComponentScan public class SpringBootApp1 { public static void main(String[]
 * args) { SpringApplication.run(SpringBootApp1.class, args); }
 * 
 * }
 */