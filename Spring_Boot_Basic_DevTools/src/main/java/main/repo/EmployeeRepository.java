package main.repo;
import org.springframework.data.repository.CrudRepository;

import main.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee,Integer >{

}
