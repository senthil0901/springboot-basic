package main.dao;

import main.model.Employee;

public interface EmployeeDaoIntf {

    void saveData(Employee employee);

    Employee fetchById(int id);

}
