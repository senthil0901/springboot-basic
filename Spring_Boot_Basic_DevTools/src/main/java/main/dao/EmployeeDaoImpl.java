package main.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import main.model.Employee;
import main.repo.EmployeeRepository;

@Repository
public class EmployeeDaoImpl implements EmployeeDaoIntf{

   /*@Autowired
   SessionFactory sessionFactory;*/
    @Autowired
    EmployeeRepository repo;
    public void saveData(Employee employee) {
        System.out.println(employee.getName());
        repo.save(employee);
        
        //sessionFactory.openSession().save(employee);   
        
    }
    @Override
    public Employee fetchById(int id) {
        Employee emp = null;
       try
       {
     emp= repo.findById(id).orElseThrow(() -> new  EmployeeIdNotFoundException("Employee not found - " + id));
    
       }
       catch(EmployeeIdNotFoundException e)
       {
           System.out.println(e);
       }
    return emp;
    }

}
