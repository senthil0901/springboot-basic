package main.service;

import main.model.Employee;

public interface EmployeeServiceIntf {

    void saveData(Employee employee);

    Employee fetchById(int id);

}
