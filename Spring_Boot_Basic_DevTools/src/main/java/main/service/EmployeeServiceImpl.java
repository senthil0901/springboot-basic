package main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import main.dao.EmployeeDaoIntf;
import main.model.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeServiceIntf {

    @Autowired
    EmployeeDaoIntf edi;
    public void saveData(Employee employee) {
       edi.saveData(employee); 
        
    }
    @Override
    public Employee fetchById(int id) {
        // TODO Auto-generated method stub
        return edi.fetchById(id);
    }

}
