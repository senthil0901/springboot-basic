package main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import main.model.Employee;
import main.service.EmployeeServiceIntf;

@RestController
public class sampleController {
    @Autowired
    EmployeeServiceIntf esi;
	@RequestMapping(value = "/newEmployee")
	public ModelAndView empform(@ModelAttribute("employee") Employee employee) {
		return new ModelAndView("addForm");
	}
	@RequestMapping(value = "/save")
    public ModelAndView saveData(@ModelAttribute("employee") Employee employee) {
	    esi.saveData(employee);
        return new ModelAndView("Success");
    }
	@RequestMapping(value = "/findById/{id}")
    public Employee fetchById(@PathVariable("id") int id) {
        
      Employee emp=esi.fetchById(id);
      return emp;
    }
}
