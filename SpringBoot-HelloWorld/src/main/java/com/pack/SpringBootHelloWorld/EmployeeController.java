package com.pack.SpringBootHelloWorld;



import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController 
{
   @GetMapping("/hello")
    public String getHello() 
    {
	   System.out.println("hello");
      return "Hello World from EmployeeController";
    }
   
   @RequestMapping("/emp")
   public List<Employee> getEmployees() 
   {
     List<Employee> employeesList = new ArrayList<Employee>();
     employeesList.add(new Employee(1,"Ram","Kumar","ram@gmail.com"));
     return employeesList;
   }
}