package com.pack.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

	   @GetMapping("/hello1")
	    public String getHello() 
	    {
		   System.out.println("hello");
	      return "Welcome to Spring Boot";
	    }
}
