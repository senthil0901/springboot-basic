package com.pack.SpringBootJetty;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

	   @GetMapping("/hello1")
	    public String getHello() 
	    {
	      return "Welcome to Spring Boot Using Jetty";
	    }
}
