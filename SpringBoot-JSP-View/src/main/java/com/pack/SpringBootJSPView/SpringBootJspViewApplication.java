package com.pack.SpringBootJSPView;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJspViewApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJspViewApplication.class, args);
	}

}


//Run as https://localhost:8443/MVC